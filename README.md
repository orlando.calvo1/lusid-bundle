
[![pypi](https://img.shields.io/pypi/v/lusid-bundle)](https://pypi.org/project/lusid-bundle/)
[![python](https://img.shields.io/pypi/pyversions/lusid-bundle.svg)](https://pypi.org/project/lusid-bundle/)

# 1. lusid-bundle
#####  *`lusid-bundle` is a python package that makes it quick and easy to get started using Lusid and Luminesce. It bundles all packages needed to get started.*






##  1. <a name='Conveniencepackageinstallations'></a>Convenience package installations

```mermaid
graph LR;
    lusid_bundle --> luminesce_sdk_preview
    lusid_bundle --> lusid_jam
    lusid_bundle --> lusid_sdk_preview
    lusid_bundle --> fbnlab_preview
    lusid_bundle --> finbourne_access_sdk
    lusid_bundle --> finbourne_identity_sdk
    lusid_bundle --> finbourne_insights_sdk_preview
    lusid_bundle --> finbourne_sdk_utilities
    lusid_bundle --> lusid_configuration_sdk_preview
    lusid_bundle --> lusid_drive_sdk_preview
    lusid_bundle --> lusid_notifications_sdk_preview
    lusid_bundle --> lusid_scheduler_sdk_preview
    lusid_bundle --> lusid_workflow_sdk_preview
    lusid_bundle --> lusidtools
    lusid_bundle --> dve_lumipy_preview

    luminesce_sdk_preview["luminesce-sdk-preview==1.14.758"]
    lusid_jam["lusid-jam==0.1.2"]
    lusid_sdk_preview["lusid-sdk-preview==1.1.120"]
    fbnlab_preview["fbnlab-preview==0.1.108"]
    finbourne_access_sdk["finbourne-access-sdk==0.0.3751"]
    finbourne_identity_sdk["finbourne-identity-sdk==0.0.2834"]
    finbourne_insights_sdk_preview["finbourne-insights-sdk-preview==0.0.763"]
    finbourne_sdk_utilities["finbourne-sdk-utilities==0.0.10"]
    lusid_configuration_sdk_preview["lusid-configuration-sdk-preview==0.1.514"]
    lusid_drive_sdk_preview["lusid-drive-sdk-preview==0.1.617"]
    lusid_notifications_sdk_preview["lusid-notifications-sdk-preview==0.1.923"]
    lusid_scheduler_sdk_preview["lusid-scheduler-sdk-preview==0.0.829"]
    lusid_workflow_sdk_preview["lusid-workflow-sdk-preview==0.1.810"]
    lusidtools["lusidtools==1.0.14"]
    dve_lumipy_preview["dve-lumipy-preview==0.1.1075"]

```
